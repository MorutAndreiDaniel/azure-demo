#General variables

variable "resource_group_name" {
  description = "Name of the resource group where the project resources are created"
  type        = string
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "project" {
  description = "Name of the project"
  type        = string
  default     = "demo"
}

variable "subscription_id" {
  description = " Subscription ID where the resource group for the project it's located"
  type        = string
  sensitive   = true
}

variable "tenant_id" {
  description = "Tenant ID of the subscription of the project"
  type        = string
  sensitive   = true
}

variable "location" {
  description = "The location of the resources to create. Defaults to the location of the resource group."
  type        = string
  default     = null
}

#VNET variables
variable "use_for_each" {
  type    = bool
  default = true
}
variable "address_space" {
  type        = list(string)
  description = "The address space that is used by the virtual network."
  default     = ["10.3.0.0/16"]
}



variable "subnet" {
  type = object({
    vm_subnet = object({
      name = string
      cidr = string
    })
  })
  default = {
    vm_subnet = {
      name = "vm_subnet"
      cidr = "10.3.0.0/17"
    }
  }
}

#VMs variables
variable "vms_count" {
  type        = number
  description = "List of vms number"
}

variable "os_disk_caching" {
  description = "The Type of Caching which should be used for the Internal OS Disk. Possible values are None, ReadOnly and ReadWrite."
  type        = string
  default     = "ReadWrite"
}

variable "os_disk_sa_type" {
  description = "The Type of Storage Account which should back this the Internal OS Disk. Possible values are Standard_LRS, StandardSSD_LRS, Premium_LRS, StandardSSD_ZRS and Premium_ZRS."
  type        = string
  default     = "Standard_LRS"
}



variable "image_attributes" {
  type = object({
    linux_image = object({
      publisher = string
      offer     = string
      sku       = string
      version   = string
    })
  })
  default = {
    linux_image = {
      offer     = "0001-com-ubuntu-server-jammy"
      publisher = "Canonical"
      sku       = "22_04-lts"
      version   = "22.04.202205060"
    }
  }
}
