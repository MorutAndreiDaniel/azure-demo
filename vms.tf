resource "azurerm_network_interface" "vm_network_interface" {
    count = var.vms_count
  name                = "${local.project}-${local.environment}-nic-${count.index}"
  location            = var.location
  resource_group_name = var.resource_group_name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = module.vnet.vnet_subnets_name_id["vm_subnet"]
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.vms_public_ip[count.index].id
  }
}

resource "azurerm_public_ip" "vms_public_ip" {
  count = var.vms_count
  name                = "${local.project}-${local.environment}-publicIP${count.index}"
  resource_group_name = var.resource_group_name
  location            = var.location
  allocation_method   = "Static"

   tags = local.resource_tags
}

resource "random_password" "vm_admin_password" {
    count = var.vms_count
  length           = 16
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

resource "azurerm_linux_virtual_machine" "linux_vms" {
    count = var.vms_count
  name                = "vm-${count.index}"
  resource_group_name = var.resource_group_name
  location            = var.location
  size                = "Standard_F2"
  admin_username = "admin-${count.index}"
  admin_password = random_password.vm_admin_password[count.index].result
  disable_password_authentication = false
  network_interface_ids = [
    azurerm_network_interface.vm_network_interface[count.index].id,
  ]

  os_disk {
    caching              = var.os_disk_caching
    storage_account_type = var.os_disk_sa_type
  }

  source_image_reference {
    publisher = var.image_attributes.linux_image.publisher
    offer     = var.image_attributes.linux_image.offer
    sku       = var.image_attributes.linux_image.sku
    version   = var.image_attributes.linux_image.version
  }

  connection {
    type     = "ssh"
    user     = "root"
    password = "${var.root_password}"
    host     = "${var.host}"
  }

  tags = local.resource_tags
}

resource "null_resource" "ping_code" {
  count = var.vms_count
  
  triggers = {
    always_run = "${timestamp()}"
  }

connection {
    type     = "ssh"
    user     = azurerm_linux_virtual_machine.linux_vms[count.index].admin_username
    password = azurerm_linux_virtual_machine.linux_vms[count.index].admin_password
    host     = azurerm_linux_virtual_machine.linux_vms[count.index].private_ip_address
  }

  provisioner "local-exec" {
    command = "ping -c 5 ${azurerm_linux_virtual_machine.linux_vms[count.index].public_ip_address} >> ${path.module}/ping_outputs.txt"
  }

}

