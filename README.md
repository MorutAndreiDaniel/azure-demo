# Azure Demo

The project is divided into a few terraform scripts:
    - main.tf where the providers are set and also some locals
    - terraform.tfvars where some of the variables are set with explicit values
    - variables.tf containing all the variables used. Also some of them have default defined which are used
    - vms.tf consists of multiple resources related to creation of vms:
        - network interface for every vm
        - public IP used for vms and referenced in network interface
        - random password for admin user created on the vms
        - linux vms
        - null resource which use a connection block to connect to the vms and local-exec provisioner that has a command for pinging at a certain IP and setting the result in a file on local that can be later referenced
    - vnet.tf consists of a module that creates a virtual network and a subnet
