terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
    }

    azuread = {
      source = "hashicorp/azuread"
    }
  }

}

provider "azurerm" {
  subscription_id = var.subscription_id
  tenant_id       = var.tenant_id
  features {}
}

provider "azuread" {
  tenant_id = var.tenant_id
}

locals {
  resource_tags = merge(var.tags, {
    Terraform   = "true"
    project     = var.project
    environment = terraform.workspace
  })

  environment = terraform.workspace
  project     = var.project
}
