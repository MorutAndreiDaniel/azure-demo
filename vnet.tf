module "vnet" {
  source              = "Azure/vnet/azurerm"
  use_for_each        = var.use_for_each
  resource_group_name = var.resource_group_name
  vnet_name           = "${local.project}-${local.environment}-VNET"
  address_space       = var.address_space
  vnet_location       = var.location
  subnet_prefixes = [
    var.subnet.vm_subnet.cidr
  ]

  subnet_names = [
    var.subnet.vm_subnet.name
  ]

  tags = local.resource_tags
}
